package com.chavezedgar.ventabalones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.chavezedgar.ventabalones.Activitys.CarritoActivity;
import com.chavezedgar.ventabalones.Activitys.ContactanosActivity;
import com.chavezedgar.ventabalones.Activitys.ProductosActivity;

public class MenuActivity extends AppCompatActivity {

    Button btnSalir, btnProductos,btnCarrito, btnContactanos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        btnSalir = findViewById(R.id.btnSalir);
        btnProductos = findViewById(R.id.btnVerBalones);
        btnContactanos = findViewById(R.id.btnContactanos);
        btnCarrito = findViewById(R.id.btnCarrito);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, MainActivity.class));
                finish();
            }
        });

        btnContactanos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, ContactanosActivity.class));
            }
        });

        btnProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, ProductosActivity.class));
            }
        });

        btnCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, CarritoActivity.class));
            }
        });

    }
}