package com.chavezedgar.ventabalones.BD;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class OpenHelper extends SQLiteOpenHelper {

    public OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Carrito(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre text, precio double, cantidad int)");
        db.execSQL("create table Productos(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre text, precio double, cantidad int, foto text)");
        db.execSQL("CREATE TABLE Usuario(id INTEGER PRIMARY KEY AUTOINCREMENT, usuario text, contrasenia text)");
        db.execSQL("INSERT INTO Usuario (usuario, contrasenia) VALUES ('Chavez', '123456')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS Carrito");
        db.execSQL("DROP TABLE IF EXISTS Usuario");
        onCreate(db);
    }

    //checking if the username exists
    public Boolean chkUsername(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from Usuario where usuario=?", new String[]{username});
        if (cursor.getCount() > 0)
            return false;
        else
            return true;
    }

    //checking the username and the password
    public Boolean userpassw(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from Usuario where usuario=? and contrasenia=?", new String[]{username, password});
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }

    }
}
