package com.chavezedgar.ventabalones.Adaptador;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.chavezedgar.ventabalones.Objeto.Balones;
import com.chavezedgar.ventabalones.R;
import com.squareup.picasso.Picasso;
import java.util.List;

public class BalonesAdapter
        extends RecyclerView.Adapter<BalonesAdapter.BalonesViewHolder>
            implements View.OnClickListener{

    List<Balones> listaBalones;
    private View.OnClickListener listener;

    public BalonesAdapter(List<Balones> listaBalones) {
        this.listaBalones = listaBalones;
    }

    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public BalonesAdapter.BalonesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist_balones, parent, false);
        view.setOnClickListener(this);
        return new BalonesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BalonesAdapter.BalonesViewHolder holder, int position) {
        holder.tv_tipoBalon.setText(listaBalones.get(position).getNombre());
        holder.tv_precio.setText(String.valueOf(listaBalones.get(position).getPrecio()));
        Picasso.get().load(listaBalones.get(position).getImagen()).into(holder.ivBalon);
    }

    @Override
    public int getItemCount() {
        return listaBalones.size();
    }

    public class BalonesViewHolder extends RecyclerView.ViewHolder {
        ImageView ivBalon;
        TextView tv_tipoBalon, tv_precio;

        public BalonesViewHolder(View itemView) {
            super(itemView);
            ivBalon = itemView.findViewById(R.id.iv_balones);
            tv_tipoBalon = itemView.findViewById(R.id.tv_nombre);
            tv_precio = itemView.findViewById(R.id.tv_precio);
        }
    }
}
