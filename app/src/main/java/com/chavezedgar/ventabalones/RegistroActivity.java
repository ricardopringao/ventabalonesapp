package com.chavezedgar.ventabalones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chavezedgar.ventabalones.BD.OpenHelper;

public class RegistroActivity extends AppCompatActivity {

    EditText txtUsuario, txtPass;
    Button btnRegist;
    String user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        txtUsuario = findViewById(R.id.txtUser);
        txtPass = findViewById(R.id.txtPass);
        btnRegist = findViewById(R.id.btnregist);

        btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = txtUsuario.getText().toString();
                pass = txtPass.getText().toString();

                OpenHelper conexion = new OpenHelper(getApplicationContext(), "Balones", null, 1);
                SQLiteDatabase db = conexion.getWritableDatabase();

                if (!user.isEmpty() && !pass.isEmpty()){
                    try {
                        ContentValues valores = new ContentValues();
                        valores.put("usuario", user);
                        valores.put("contrasenia", pass);
                        db.insert("Usuario", null, valores);
                        db.close();
                        Toast.makeText(getApplicationContext(), "Se ha registrado con exito", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                        startActivity(intent);

                    }catch (Exception e){
                        Log.e("DBSQLITE", e.getMessage());
                    }
                }else if (user.isEmpty()){
                    txtUsuario.setError("Campo Obligatorio");
                    txtUsuario.requestFocus();
                }else if (pass.isEmpty()){
                    txtPass.setError("Campo Obligatorio");
                    txtPass.requestFocus();
                }
            }
        });

    }
}