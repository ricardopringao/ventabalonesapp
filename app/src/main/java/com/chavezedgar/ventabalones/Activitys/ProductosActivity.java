package com.chavezedgar.ventabalones.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.chavezedgar.ventabalones.Adaptador.BalonesAdapter;
import com.chavezedgar.ventabalones.Objeto.Balones;
import com.chavezedgar.ventabalones.R;

import java.util.ArrayList;

public class ProductosActivity extends AppCompatActivity {

    RecyclerView rvBalones;
    ArrayList<Balones> listaBalones;
    Balones b;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        listaBalones = new ArrayList<>();
        rvBalones = findViewById(R.id.recyclerBalones);
        mostrarData();

    }

    private void mostrarData() {

        b = new Balones();
        b.setNombre("Balon de Futbol");
        b.setPrecio(20.00);
        b.setImagen("https://images-na.ssl-images-amazon.com/images/I/61ro5uS6hEL._AC_SY450_.jpg");
        listaBalones.add(b);

        b = new Balones();
        b.setNombre("Balon de Basket");
        b.setPrecio(20.00);
        b.setImagen("https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Basketball.png/220px-Basketball.png");
        listaBalones.add(b);

        b = new Balones();
        b.setNombre("Balon de Tenis");
        b.setPrecio(20.00);
        b.setImagen("https://images-na.ssl-images-amazon.com/images/I/412S4qEMgOL._AC_SX355_.jpg");
        listaBalones.add(b);

        rvBalones.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        rvBalones.setHasFixedSize(true);
        BalonesAdapter adaptador = new BalonesAdapter(listaBalones);
        rvBalones.setAdapter(adaptador);

        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = listaBalones.get(rvBalones.getChildAdapterPosition(view)).getNombre();
                Double precio = listaBalones.get(rvBalones.getChildAdapterPosition(view)).getPrecio();
                String urlImagen = listaBalones.get(rvBalones.getChildAdapterPosition(view)).getImagen();
                Intent intent = new Intent(getApplicationContext(), DetalleActivity.class);
                intent.putExtra("nombre", nombre);
                intent.putExtra("urlImagen", urlImagen);
                intent.putExtra("precio", precio);
                startActivity(intent);
            }
        });

    }
}