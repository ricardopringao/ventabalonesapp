package com.chavezedgar.ventabalones.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.chavezedgar.ventabalones.BD.OpenHelper;
import com.chavezedgar.ventabalones.MenuActivity;
import com.chavezedgar.ventabalones.Objeto.Carrito;
import com.chavezedgar.ventabalones.R;

import java.util.ArrayList;

public class CarritoActivity extends AppCompatActivity {

    ListView lv_carrito;
    ArrayList<Carrito> listCarrito;
    ArrayList<String>  listaInfo;
    Button btnComprar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);

        lv_carrito = findViewById(R.id.lv_carrito);
        btnComprar = findViewById(R.id.btnComprar);

        consultaSQlite();

        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenHelper conexion = new OpenHelper(getApplicationContext(), "Balones", null, 1);
                SQLiteDatabase db = conexion.getReadableDatabase();
                db.delete("Carrito", null,null);
                db.close();
                Toast.makeText(getApplicationContext(), "Se ha comprado con éxito", Toast.LENGTH_SHORT);
                startActivity(new Intent(CarritoActivity.this, MenuActivity.class));
            }
        });
    }

    private void consultaSQlite() {

        OpenHelper conexion = new OpenHelper(this, "Balones", null, 1);
        SQLiteDatabase db = conexion.getReadableDatabase();

        Carrito carrito = null;
        listCarrito = new ArrayList<Carrito>();

        Cursor cursor = db.rawQuery("Select * from Carrito", null);

        while(cursor.moveToNext()){
            carrito = new Carrito();
            carrito.setId(cursor.getInt(0));
            carrito.setNombre(cursor.getString(1));
            carrito.setPrecio(cursor.getDouble(2));
            carrito.setCantidad(cursor.getInt(3));
            listCarrito.add(carrito);
        }

        mostrarLista();

    }

    private void mostrarLista() {
        listaInfo = new ArrayList<String>();

        for (int i = 0; i < listCarrito.size(); i++) {
            listaInfo.add(listCarrito.get(i).getNombre() + " - "
                    + listCarrito.get(i).getCantidad() + " - $" + listCarrito.get(i).getPrecio());

            ArrayAdapter adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, listaInfo);
            lv_carrito.setAdapter(adaptador);
        }

    }

}