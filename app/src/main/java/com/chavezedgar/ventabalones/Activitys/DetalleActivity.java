package com.chavezedgar.ventabalones.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chavezedgar.ventabalones.BD.OpenHelper;
import com.chavezedgar.ventabalones.MenuActivity;
import com.chavezedgar.ventabalones.R;
import com.squareup.picasso.Picasso;

public class DetalleActivity extends AppCompatActivity {

    ImageView ivFotoBalon;
    TextView lbl_nombreBalon, lbl_Precio;
    EditText txtCantidad;
    Button btnAnadirCarrito;
    String nombre, imagen;
    Double precio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        ivFotoBalon = findViewById(R.id.ivFotoBalon);
        lbl_nombreBalon = findViewById(R.id.lbl_nombre);
        lbl_Precio = findViewById(R.id.lbl_precio);
        txtCantidad = findViewById(R.id.txtCantidad);
        btnAnadirCarrito = findViewById(R.id.btnAnadirCarrito);

        nombre = getIntent().getStringExtra("nombre");
        imagen = getIntent().getStringExtra("urlImagen");
        precio = getIntent().getDoubleExtra("precio", 0);


        lbl_nombreBalon.setText("Producto: " + nombre);
        lbl_Precio.setText("Precio: " + String.valueOf(precio));
        Picasso.get().load(imagen).into(ivFotoBalon);

        btnAnadirCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgregarAlCarro();
            }
        });


    }

    private void AgregarAlCarro() {
        OpenHelper conexion = new OpenHelper(this, "Balones", null, 1);
        SQLiteDatabase db = conexion.getWritableDatabase();
        try {
            ContentValues valores = new ContentValues();
            valores.put("nombre", nombre);
            valores.put("precio", precio);
            valores.put("cantidad", Double.parseDouble(txtCantidad.getText().toString()));
            db.insert("Carrito", null, valores);
            Toast.makeText(this, "Se agregó al carrito de compras", Toast.LENGTH_SHORT).show();
            db.close();
            startActivity(new Intent(DetalleActivity.this, MenuActivity.class));
        } catch (Exception e) {
            Log.e("DBSQLITE", e.getMessage());
        }
    }
}