package com.chavezedgar.ventabalones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chavezedgar.ventabalones.BD.OpenHelper;

public class MainActivity extends AppCompatActivity {

    EditText txtUser, txtPassword;
    Button btnLogin, registrarse;
    String usuario, contrasenia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUser = findViewById(R.id.txtUsuario);
        txtPassword = findViewById(R.id.txtContrasenia);
        btnLogin = findViewById(R.id.btnLogin);
        registrarse = findViewById(R.id.btnRegistrarse);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario = txtUser.getText().toString();
                contrasenia = txtPassword.getText().toString();

                OpenHelper db = new OpenHelper(getApplicationContext(), "Balones", null, 1);

                Boolean ChkUserPassw = db.userpassw(usuario,contrasenia);

                if(ChkUserPassw==true){
                    Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_SHORT).show();
                    Intent home = new Intent(MainActivity.this, MenuActivity.class);
                    startActivity(home);
                }
                else{
                    Toast.makeText(getApplicationContext(),"Equivocado", Toast.LENGTH_SHORT).show();
                }

            }
        });

        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });

    }
}